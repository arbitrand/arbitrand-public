# Randomness Tests

This repository contains three tests under the `rng_tests` folder:
- TestU01
- PractRand
- The NIST statistical test suite

TestU01 is the go-to test suite for Arbitrand.  It chews through a lot of data fairly quickly (1 terabyte in 4 hours), and has a large suite of tests that check many different aspects of randomness.  PractRand is used for testing larger data volumes, and is also a
very stringent test suite.  The NIST STS is a slightly weaker test suite is best used on small data volumes, but has a lot of useful randomness tests.

## TestU01

Run `make` inside `rng_tests` to compile `arbitrand_testu01`.

The `arbitrand_testu01` program can be used for testing the quality of any random number generator available as a Linux file.  The program takes 2 arguments, with an optional third argument:

Usage: `arbitrand_testu01 [test size] [optional: path] [optional: fold factor]`

The first argument, test size, describes the size of the test to run:
- "small" or "smallCrush" runs a quick test suite that is more stringent than the NIST STS suite, but takes only a few minutes.
- "crush" runs an hour-long test suite on several gigabytes of random data.
- "big" or "bigCrush" runs a test suite that tests a terabit of random data, and takes about 4 hours to run.  BigCrush is part of Arbitrand's qualification process.
- "linear" runs a suite of linear complexity tests on the data.

The path is the file path to test.  The default path is `/dev/arbitrand0`, which tests one of the FPGAs attached to your machine.  You can test the other FPGAs (if you have more than one) by setting a different path.  You can also test `/dev/random` if you so desire.

The fold factor allows you to "fold" the data going into the tester by XORing it with itself.   A high fold factor lets you test more data, but it improves the quality of the RNG by feeding more entropy into each bit tested.

## PractRand

Run `build.sh` inside `rng_tests/practrand` to compile.  PractRand provides 3 programs, but the relevant one for testing is `RNG_test` (the other two allow you to benchmark and inspect the output of pseudorandom number generators).

To test a linux file like `/dev/arbitrand0`, the easiest method is: `cat [filename] | RNG_test stdin`

By default, PractRand will continue running until it tests 32 TB of output, which takes about 8 days on an f1.2xlarge instance using this method.

## NIST Statistical Test Suite

1. Run `make` inside `rng_tests/nist-sts-2.1.2` to build the test program, which is named `assess`.
2. Source `create-dir-script` under `rng_tests/nist-sts-2.1.2/experiments`

From there, you can run `./assess n` to test streams of n bits of data and follow the instructions for testing an input file.

This test suite doesn't scale up as well as the other test suites.  The common practice for this test suite is to test several streams of about a megabit, rather than to test longer bit streams.  You certainly can test longer bit streams, but
there is no guarantee that the parameters of the test suite are set up for those tests (although some of them do scale up).  Testing a few megabits takes minutes.

The sequence of inputs to run the full test suite is: `0` (run an input file), `[filename]` (select the file), `1` (run all the tests), `0` (don't modify any parameters), `s` (number of bitstreams to test), `1` (binary input)

Once the test is complete, the results will be produced in `rng_tests/nist-sts-2.1.2/experiments/AlgorithmTesting`.  You will probably want the file named `finalAnalysisReport.txt`, but separate reports for every individual test are produced.

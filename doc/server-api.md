# The Randomness Server and its API

The server handles random number distribution using HTTP, HTTPS, and TCP.  It takes its configuration from a configuration JSON file.  By default, the server accepts HTTP requests on port 4900 and TCP requests on port 4902.

Inside the `server` folder there is some go code for the server.  Run `go build` to build a new
version of the server.

## Configuration JSON

The configuration file is a JSON-formatted file.

The example configuration files included are a good starting point for each type of AWS server.  If you want to use HTTPS within your deployment, you will need to issue certificates for the machine.

#### HTTP Configuration

You can enable or disable HTTP and set the port number.

| Name | JSON Tag | Type | Description |
| --- | --- | --- | --- |
| Port | `port` | Integer | Port number to use |
| Enabled | `enabled` | Boolean | Whether to enable the interface or leave it disabled |

#### HTTPS Configuration

The HTTPS endpoint has extra configuration options so that you can provide a certificate for the URL that you assign to the server.

| Name | JSON Tag | Type | Description |
| --- | --- | --- | --- |
| Port | `port` | Integer | Port number to use |
| Enabled | `enabled` | Boolean | Whether to enable the interface or leave it disabled |
| Certificate File | `certfile` | String | Path of the file that contains the SSL certificate to use |
| Private Key File | `keyfile` | String | Path of the file that contains the private key to use |

#### TCP Configuration

You can enable or disable TCP and set the port number.

| Name | JSON Tag | Type | Description |
| --- | --- | --- | --- |
| Port | `port` | Integer | Port number to use |
| Enabled | `enabled` | Boolean | Whether to enable the interface or leave it disabled |

#### General Configuration

The general configuration options control parameters related to how the random number generators are read.  It is probably a good idea to use the default values for your instance type unless you have a reason to switch.

| Name | JSON Tag | Type | Description |
| --- | --- | --- | --- |
| Threads per RNG | `ThreadsPerRNG` | Integer | Number of reader threads that can access each random number generator at any given time |
| RNG File Names | `RngFiles` | List of Strings | List of file names for random number generators |

Threads per RNG can be tuned based on the size of requests you are serving.  The "RNG File Names" field should include the file names of every random number generator attached to the system.  By default, we use `/dev/arbitrand_serve[n]` for each FPGA
rather than `/dev/arbitrand[n]`.  This allows your local programs to use a separate device queue than the server, which should improve isolation.  **If you change instance types for higher throughput, this list will also need to change**.

## HTTP/HTTPS API

The HTTP API lets you get random data and random numbers in a nice format for webservers and web services, but will not allow you to saturate the output bandwidth of the network card.  By default, the HTTP API is open at
port 4900.  The HTTPS endpoint is assigned to port 4901, but for the AWS AMI you need to provide certificate and private key files (matching the domain name you assign to the server) to enable HTTPS.  The config JSON has places to
put these files.

There are three endpoints on the HTTP API:
- `/data` returns random data generated and formatted according to your specifications
- `/num` returns a random numbers (ASCII formatted) according to your specifications
- `/status` returns the current status of the randomness server
Arguments should be passed as an HTTP query, and all requests should be `GET` requests.

### The Random Data Endpoint

The `/data` endpoint accepts `GET` requests for data with the following query parameters:

| Name | Query Field | Description | Valid Values | Default Value |
| --- | --- | --- | --- | --- |
| Length | `len` or `l` | Number of raw bytes of data to get | less than 2 MB | 64 bytes |
| Encoding | `encoding`, `enc`, or `e` | Encoding to use in transit | Base64 (`base64` or `b64`), hexadecimal (`h`, `hex`, or `hexadecimal`), or raw binary (`raw`) | `base64` |

Be aware that if you send a large request to this endpoint, the server will allocate at least as much RAM as you have requested (twice as much for base64 and 3x as much for hexadecimal).  It is not recommended to use the
`/data` HTTP endpoint for requests over a few megabytes. If you do want to use the `/data` endpoint for these requests, raw binary encoding is strongly suggested.  Having a few outstanding gigabyte-sized requests should
work fine, but will not be very resource-efficient.

#### Usage Examples

- `/data?len=32&enc=base64` would return 32 bytes of base64 encoded data
- `/data?l=1000000&enc=raw` would return a megabyte of raw binary data
- `/data?encoding=hex` would return 64 bytes of data encoded in hex
- `/data` would return 64 bytes of data encoded in base64

### The Random Number Endpoint

The `/num` endpoint accepts `GET` requests for numbers with the following parameters, and returns a space-separated list of random integers in the range $[floor, limit)$, encoded in ASCII:

| Name | Query Field | Description | Valid Values | Default Value |
| --- | --- | --- | --- | --- |
| Floor | `floor` or `f` | Floor of the range of the random number you want generated (inclusive) | $-2^{63}$ to $2^{63} - 1$ | 0 |
| Limit | `limit`, `lim`, or `l` | Limit of the range of random number you want generated (exclusive) | $-2^{63}$ to $2^{63} - 1$ | $2^{63} - 1$ |
| Base | `base` | Base of all numbers used | 2-36 | 10 |
| Count | `count` or `draws` | Number of times to run | 0-32768 | 1 |
| Redraw | `redraw` | Whether to allow repeated outputs when count > 1 | `true` or `false` | `true` |

If count is set greater than 1 and redraw is false, there will be no
repeats in the list, but if redraw is true, the list may contain
repeated values.

Additionally, the distance between the floor and the limit can be no more than $2^{63} - 1$.

#### Usage Examples

- `/num` returns a random positive integer in base 10 less than $2^{63} - 1$
    - Example output: `3478682698`
- `/num?floor=1&limit=7` simulates a roll of a 6-sided die
    - Example output: `4`
- `/num?f=-10&l=11` returns a random integer whose absolute value is 10 or less (valid values are 10, 9, 8, ..., -8, -9, -10)
    - Example output: `-2`
- `/num?floor=0&lim=f00d&base=16` would return a random positive hexadecimal number less than 61453 (`0xf00d`)
    - Example output: `2a5b`
- `/num?lim=2` simulates a coin flip (returning either 0 or 1)
    - Example output: `0`
- `/num?lim=2&count=10` flips 10 coins
    - Example output: `0 1 0 1 1 0 1 0 0 1`
- `/num?f=1&l=81&count=20&redraw=false` simulates a game of Keno (draws 20 numbers from 1-80 inclusive with no repeats)
    - Example output: `5 39 2 46 42 20 12 63 80 75 57 1 34 27 26 17 55 60 1 72`

### The Status Endpoint

The randomness server runs continuous tests of its internal machinery to ensure that everything is
correct.  It will disable itself if the TRNG is not behaving well (in case of hardware issues).
The status endpoint is available to show if there are any problems with the randomness of the machine
and indicate the current status of continuous testing.

The status endpoint takes no arguments, and returns a JSON with the following fields:
- `Names`: The names of the TRNG files used by the server
- `Correctness`: A correctness score for continuous tests, with results of the last 20 tests
- `LastFailed`: The time of the last continuous test failure

If the correcntess score of any TRNG is below 0.9 (indicating 2 test failures in the last 20), we
automatically disable that TRNG.  It is recommended that if this happens and you need random numbers
immediately, you restart the server.  Occasional spurious test failures are expected (this is a
random number generator), but 2 failures in the last 20 is considered anomalous.

## TCP API

The TCP API is designed to provide random data to your server with minimum overhead.  If you need high throughput, the server should be able to saturate its available output bandwidth with random bits over TCP.

To use the TCP API:
- Open a TCP connection to the server's TCP port (default 4902)
- Send 4-byte unsigned (little endian) integers to the server to request data
- The server will give you back that many bytes of random data
- Repeat as many times as needed
- Send a "0" or close the connection when you are done

You can send many 4-byte requests per connection, and you do not have to wait for one request to finish to initiate another one.  The maximum amount of data that can be requested at once is just under 4 GiB.
Unlike with the HTTPS API, the server is designed to accept large requests over TCP, and will handle them without significant RAM pressure.

The server will close inactive TCP connections that have not send a request in the last 15 minutes.  Infrequently-used connections can be kept alive with 1-byte requests.
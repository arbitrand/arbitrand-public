# Arbitrand Public Repository

This repository contains open-source code for every program that appears on an Arbitrand AMI.

## The Remote Randomness Server

Inside the `server` folder there is some go code for a server which provides random numbers over TCP and HTTP(S).  Run `go build` inside the folder to build `arbitrand-server`.

Further details are available in [the server and API documentation](doc/server-api.md)

## Randomness Tests

This repository contains three tests under the `rng_tests` folder:
- The NIST statistical test suite
- TestU01
- PractRand

Further details on how to run randomness tests is included in [the docs folder](doc/testing.md)

## Contributing

You are welcome to send a pull request if you find a bug in this library.  Your code must by MIT licensed.

## License

Code produced by Arbitrand LLC in this repository is licensed under the MIT license.  This repository contains some open-source code from third parties, which is licensed accordingly.

Third-party code is found in the directories:
- `rng_tests/nist-sts-2.1.2`
- `rng_tests/practrand`
- `rng_tests/TestU01`
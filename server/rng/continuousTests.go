package rng

// Package with a few basic tests to continuously run to make sure that RNGs are
// still working.

import (
	"math"

	pc "github.com/tmthrgd/go-popcount"
)

// Helper functions
// Bit proportion
func bitProp(data []byte) float64 {
	popcount := pc.CountBytes(data)
	return float64(popcount) / float64(len(data)*8)
}

// Desired statistical significance
const SIGNIFICANCE = 0.01

// NIST monobit test
func freqTest(data []byte) bool {
	popcount := pc.CountBytes(data)
	test_stat := math.Abs(2*float64(popcount)-float64(len(data)*8)) / math.Sqrt(float64(len(data)*8))
	p_value := math.Erfc(test_stat / math.Sqrt(2.0))
	return p_value > SIGNIFICANCE
}

// NIST runs test
func runsTest(data []byte) bool {
	var v = 1
	var combined uint16 = uint16(data[0])
	for _, b := range data[1:] {
		combined = combined | (uint16(b) << 8)
		for i := 0; i < 8; i++ {
			if (combined&0x3) == 0x1 || (combined&0x3) == 0x2 {
				v += 1
			}
			combined = combined >> 1
		}
	}

	prop := bitProp(data)
	num := math.Abs(float64(v) - 2.0*prop*(1.0-prop)*float64(len(data)*8))
	denom := 2.0 * math.Sqrt(float64(len(data)*16)) * prop * (1 - prop)
	p_value := math.Erfc(num / denom)
	return p_value > SIGNIFICANCE
}

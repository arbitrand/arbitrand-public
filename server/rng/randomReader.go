package rng

import (
	"encoding/binary"
	"errors"
	"log"
	"os"
	"sync"
	"time"
)

type RandomBufferedReader struct {
	rng        *os.File
	buffer     []byte
	ptr        int
	mut        sync.Mutex
	cycledTime time.Time
}

const bufferSize = 1 << 16
const largeReadSize = 1 << 15

// Refresh the buffer at a regular interval
const bufferCycle = 100 * time.Millisecond

func min_int(a, b int) int {
	if a < b {
		return a
	}
	return b
}

// Helper functions to fill buffers and refill saved buffer
func (reader *RandomBufferedReader) FillBuffer(buf []byte) error {
	var bytes_written = 0
	for bytes_written < len(buf) {
		n, err := reader.rng.Read(buf[bytes_written:])
		if err != nil {
			return err
		}
		if n == 0 {
			return errors.New("Read 0 bytes from the RNG!")
		}
		bytes_written += n
	}

	return nil
}

func (reader *RandomBufferedReader) Refill() error {
	err := reader.FillBuffer(reader.buffer)
	if err != nil {
		return err
	}
	reader.ptr = 0
	reader.cycledTime = time.Now()
	return nil
}

// Read bytes from the buffered reader
func (reader *RandomBufferedReader) ReadBytes(out []byte) error {
	reader.mut.Lock()
	defer reader.mut.Unlock()

	// Bypass the buffer for reads that are large enough - no need to maintain ordering of random data
	if len(out) > largeReadSize {
		if err := reader.FillBuffer(out); err != nil {
			return err
		}
		return nil
	}

	if time.Now().Sub(reader.cycledTime) > bufferCycle {
		reader.Refill()
	}

	var outptr = 0
	for outptr < len(out) {
		// Take either the full amount of bytes requested or one full buffer
		bytes_requested := len(out) - outptr
		bytes_available := len(reader.buffer) - reader.ptr
		bytes_to_read := min_int(bytes_available, bytes_requested)

		copy(out[outptr:outptr+bytes_to_read], reader.buffer[reader.ptr:reader.ptr+bytes_to_read])

		// Update our read pointer and output pointer, and read more data if needed
		reader.ptr += bytes_to_read
		outptr += bytes_to_read

		if reader.ptr >= len(reader.buffer) {
			err := reader.Refill()
			if err != nil {
				return err
			}
		}
	}

	return nil
}

// Open and close the backing file
func (reader *RandomBufferedReader) Setup(filename string) error {
	reader.mut.Lock()
	defer reader.mut.Unlock()

	var err error
	reader.rng, err = os.Open(filename)
	if err != nil {
		return err
	}

	reader.buffer = make([]byte, bufferSize)
	return reader.Refill()
}

func (reader *RandomBufferedReader) Close() {
	reader.mut.Lock()
	defer reader.mut.Unlock()

	reader.rng.Close()
}

// Functions to satisfy the Source and Source64 interfaces for the Go rand package
func (reader *RandomBufferedReader) Seed(seed int64) {
	return
}

func (reader *RandomBufferedReader) Uint64() uint64 {
	var raw_buf [8]byte
	err := reader.ReadBytes(raw_buf[:])
	if err != nil {
		log.Fatalf("Internal reader error: %s", err.Error())
	}
	return binary.LittleEndian.Uint64(raw_buf[:])
}

func (reader *RandomBufferedReader) Int63() int64 {
	return int64(reader.Uint64() >> 1)
}

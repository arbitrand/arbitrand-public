package rng

type BitRing struct {
	data       []bool
	write_ptr  int
	read_ptr   int
	size       int
	running_pc int
}

func (q *BitRing) init(size int) {
	q.data = make([]bool, size+1)
}

// Push bits to the ring buffer with tail dropping
func (q *BitRing) push(bit bool) {
	// Write the data and track the running popcount
	q.data[q.write_ptr] = bit
	if bit {
		q.running_pc += 1
	}

	// If full, drop the tail, otherwise increase the size
	if q.full() {
		if q.data[q.read_ptr] {
			q.running_pc -= 1
		}
		q.read_ptr = (q.read_ptr + 1) % len(q.data)
	} else {
		q.size += 1
	}
	q.write_ptr = (q.write_ptr + 1) % len(q.data)
}

// Popcount of data between read and write pointers
func (q *BitRing) popcount() int {
	return q.running_pc
}

// Empty and full utility functions
func (q *BitRing) empty() bool {
	return q.write_ptr == q.read_ptr
}
func (q *BitRing) full() bool {
	return (q.write_ptr+1)%len(q.data) == q.read_ptr
}

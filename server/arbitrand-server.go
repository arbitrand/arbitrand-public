package main

import (
	"encoding/json"
	"log"
	"net"
	"net/http"
	"os"
	"strconv"
	"time"

	"gitlab.com/arbitrand/server/rng"
)

type HTTP struct {
	Enabled bool
	Port    int
}

type TCP struct {
	Enabled bool
	Port    int
}

type HTTPS struct {
	Enabled  bool
	Port     int
	CertFile string
	KeyFile  string
}

type Config struct {
	HTTP          HTTP
	HTTPS         HTTPS
	TCP           TCP
	ThreadsPerRng int
	RngFiles      []string
}

func main() {
	// Find configuration file
	var config_filename = "config.json"
	if len(os.Args) > 1 {
		config_filename = os.Args[1]
	}

	// Load configuration file
	log.Printf("Opening config file %s...", config_filename)
	config_json, open_err := os.ReadFile(config_filename)

	if open_err != nil {
		log.Panicf("Could not open config.json: %s", open_err)
	}

	var config Config
	if err := json.Unmarshal([]byte(config_json), &config); err != nil {
		log.Panicf("Error parsing config.json: %s", err)
	}

	if !config.HTTP.Enabled && !config.HTTPS.Enabled {
		log.Fatal("Neither HTTP nor HTTPS were enabled, exiting")
	}

	// Set up random number buffers and open the RNG files for reading
	var rng_server rng.RandomServer
	defer rng_server.Close()

	if err := rng_server.Setup(config.RngFiles, config.ThreadsPerRng); err != nil {
		log.Panicf("Error setting up RNG files: %s", err)
	}

	// Run some initial RNG tests to make sure we are ok, and launch continuous tests every 10 min
	for i := 0; i < 10; i += 1 {
		rng_server.TestRngs()
	}
	go func() {
		for {
			time.Sleep(10 * time.Minute)
			rng_server.TestRngs()
		}
	}()

	// Set up to serve responses
	errs := make(chan error)
	http.HandleFunc("/data", rng_server.ServeData)
	http.HandleFunc("/num", rng_server.ServeNumber)
	http.HandleFunc("/status", rng_server.ServeStatus)

	// Validate HTTP configuration and launch
	if config.HTTP.Enabled {
		if config.HTTP.Port <= 0 || config.HTTP.Port > 65536 {
			log.Printf("Invalid HTTP port number [%d], skipping HTTP setup", config.HTTP.Port)
		} else {
			go func() {
				log.Printf("Starting HTTP service at port %d", config.HTTP.Port)
				if err := http.ListenAndServe(":"+strconv.Itoa(config.HTTP.Port), nil); err != nil {
					errs <- err
				}
			}()
		}
	}

	// Validate HTTPS configuration and launch
	if config.HTTPS.Enabled {
		if config.HTTPS.Port <= 0 || config.HTTPS.Port > 65536 {
			log.Printf("Invalid HTTPS port number [%d], skipping HTTPS setup", config.HTTPS.Port)
		} else {
			_, cert_err := os.Stat(config.HTTPS.CertFile)
			if cert_err != nil {
				log.Printf("Error accessing certificate file [%s], skipping HTTPS setup", config.HTTPS.CertFile)
			}

			_, key_err := os.Stat(config.HTTPS.KeyFile)
			if key_err != nil {
				log.Printf("Error accessing private key file [%s], skipping HTTPS setup", config.HTTPS.KeyFile)
			}

			if key_err == nil && cert_err == nil {
				go func() {
					log.Printf("Starting HTTPS service at port %d", config.HTTPS.Port)
					err := http.ListenAndServeTLS(":"+strconv.Itoa(config.HTTPS.Port), config.HTTPS.CertFile,
						config.HTTPS.KeyFile, nil)
					if err != nil {
						errs <- err
					}
				}()
			}
		}
	}

	// Validate TCP configuration and launch
	if config.TCP.Enabled {
		if config.TCP.Port <= 0 || config.TCP.Port > 65536 {
			log.Printf("Invalid TCP port number [%d], skipping TCP setup", config.TCP.Port)
		} else {
			go func() {
				log.Printf("Starting TCP service at port %d", config.TCP.Port)
				sock, err := net.Listen("tcp", ":"+strconv.Itoa(config.TCP.Port))
				if err != nil {
					errs <- err
					return
				}

				defer sock.Close()

				for {
					c, err := sock.Accept()
					if err != nil {
						errs <- err
						return
					}

					go rng_server.HandleStream(c)
				}
			}()
		}
	}

	select {
	case err := <-errs:
		log.Printf("Could not start serving: %s", err)
	}
}

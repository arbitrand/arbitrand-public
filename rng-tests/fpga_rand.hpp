// Access to the FPGA-based random number generator
// Uses the Xilinx DMA controller to read bits from the FPGA, and stores large transfers in a
// central buffer.

#include <cstddef>

#include <iostream>
#include <string>
#include <vector>

#include <fcntl.h>
#include <unistd.h>


namespace arbitrand {

class RandomFpga {
  public:
    RandomFpga (const std::string& device_name, int fold = 1) : fold_factor(fold) {
        for (int i = 0; i < fold_factor; i++) {
            buffers.emplace_back(new char [BUFFER_SIZE]);
        }

        // Open the FPGA and fill the buffer
        fpga_fd = open(device_name.c_str(), O_RDONLY);
        if (fpga_fd < 0) {
            std::cerr << "ERROR: Could not open FPGA device " << device_name << std::endl;
            std::abort();
        }
        fill_buffers();
    }

    ~RandomFpga () {
        for (char* buffer : buffers) {
            delete buffer;
        }
        close(fpga_fd);
    }

    template<typename T>
    T get () {
        // If we do not have enough bytes to satisfy the demand, refill the buffer
        if (sizeof(T) + offset > BUFFER_SIZE) {
            fill_buffers();
        }

        T result = 0;
        for (int i = 0; i < fold_factor; i++) {
            // Get enough bytes to create a random T, reading from the buffer
            result ^= *reinterpret_cast<T*>(buffers[i] + offset);
            offset += sizeof(T);
        }
        return result;
    }

  private:
    void fill_buffers () {
        for (char* buffer : buffers) {
            std::size_t bytes_collected = 0;

            while (bytes_collected < BUFFER_SIZE) {
                int result = pread(fpga_fd, buffer + bytes_collected,
                                   BUFFER_SIZE - bytes_collected, 0);
                if (result < 0) {
                    std::cerr << "ERROR: Could not perform read from FPGA" << std::endl;
                    std::abort();
                }
                bytes_collected += result;
            }
        }

        offset = 0;
    }

    static constexpr std::size_t BUFFER_SIZE = 8 << 20;

    int fpga_fd = -1;
    int fold_factor;
    std::vector<char*> buffers;
    std::size_t offset = 0;
};

}   // namespace arbitrand

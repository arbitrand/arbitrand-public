// Test the Arbitrand RNG through TestU01

#include <iostream>
#include <memory>
#include <string>
#include <vector>

#include "fpga_rand.hpp"

extern "C" {
#include "TestU01.h"
}

std::unique_ptr<arbitrand::RandomFpga> device;

unsigned int random_device () {
    return device->get<unsigned int>();
}

int main (int argc, char** argv) {
    if(argc < 2 || argc > 4) {
        std::cerr << "Usage:" << std::endl
                  << "arbitrand_testu01 [test size] [optional: FPGA path] [optional: fold factor]" << std::endl;
        std::cerr << "Default FPGA path is /dev/arbitrand0" << std::endl;
        std::cerr << "Default fold factor is 1 (no folding of the FPGA output)" << std::endl;
        std::cerr << "Valid test suite names are: " << std::endl;
        std::cerr << "    [small / smallCrush] for the SmallCrush test suite (fast)" << std::endl;
        std::cerr << "    [crush]              for the Crush test suite (~1 hour)" << std::endl;
        std::cerr << "    [big / bigCrush]     for the BigCrush test suite (~4 hours)" << std::endl;
        std::cerr << "    [linear]             for linear complexity tests" << std::endl;
        return 1;
    }

    // Get our arguments
    std::vector<std::string> args(argv + 1, argv + argc);

    // Set up the random FPGA device and a TestU01 generator
    int fold = 1;
    std::string path = "/dev/arbitrand0";
    if (args.size() >= 2) {
        path = args[1];
    }
    if (args.size() >= 3) {
        fold = std::stoi(args[2]);
    }
    device = std::make_unique<arbitrand::RandomFpga>(path, fold);

    // Create a non-const C string out of the test name we want to use
    std::string test_name = "Arbitrand";
    std::vector<char> name_vec(test_name.begin(), test_name.end());
    name_vec.push_back(0);
    unif01_Gen* gen = unif01_CreateExternGenBits(name_vec.data(), random_device);

    // Run the specified tests
    auto& test_suite = args[0];
    if (test_suite == "small" || test_suite == "smallCrush") {
        bbattery_SmallCrush(gen);
    } else if (test_suite == "crush") {
        bbattery_Crush(gen);
    } else if (test_suite == "big" || test_suite == "bigCrush") {
        bbattery_BigCrush(gen);
    } else if (test_suite == "linear") {
        scomp_Res* res = scomp_CreateRes();
        swrite_Basic = true;
        for (int size : {250, 500, 1000, 5000, 25000, 50000, 100000}) {
            scomp_LinearComp(gen, res, 1, size, 0, 1);
        }

        scomp_DeleteRes(res);
    } else {
        std::cerr << "Unknown test suite name: " << test_suite << std::endl;
        std::cerr << "Valid options are: " << std::endl;
        std::cerr << "    [small / smallCrush] for the SmallCrush test suite (fast)" << std::endl;
        std::cerr << "    [crush]              for the Crush test suite (~1 hour)" << std::endl;
        std::cerr << "    [big / bigCrush]     for the BigCrush test suite (~4 hours)" << std::endl;
        std::cerr << "    [linear]             for linear complexity tests" << std::endl;
        return 1;
    }

    // Clean ourselves up
    unif01_DeleteExternGenBits(gen);
    return 0;
}
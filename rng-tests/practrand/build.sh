# Build script for Linux
#
# For some reason, the PractRand author does not use makefiles or anything similar, so
# this script follows his instructions for how to build PractRand

# Build the PractRand library
g++ -c src/*.cpp src/RNGs/*.cpp src/RNGs/other/*.cpp -O3 -Iinclude -pthread
ar rcs libPractRand.a *.o

# Clean up after ourselves
rm *.o

# Build RNG tests and tools
g++ -o RNG_test tools/RNG_test.cpp libPractRand.a -O3 -Iinclude -pthread
g++ -o RNG_benchmark tools/RNG_benchmark.cpp libPractRand.a -O3 -Iinclude -pthread
g++ -o RNG_output tools/RNG_output.cpp libPractRand.a -O3 -Iinclude -pthread

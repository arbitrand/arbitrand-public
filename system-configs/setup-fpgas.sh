#!/bin/bash
# Set up the FPGAs with the Arbitrand AFI.  Must be run as root.

# Find all the slots on the machine and populate them
for slot in $(sudo fpga-describe-local-image-slots | awk '$1 == "AFIDEVICE"{print $2}');
do 
	fpga-load-local-image -S $slot -I agfi-0d674c107775c3ea4 -H
done
